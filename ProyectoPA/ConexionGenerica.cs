﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;

namespace ProyectoPA
{
    class ConexionGenerica
    {
        private DbCommand comando;
        private DbConnection coneccion;
        private IDataReader read;

        public ConexionGenerica (DbConnection coneccion)
        {
            this.coneccion = coneccion;
        }

        public void open ()
        {
            try
            {
                this.coneccion.Open();
                Console.WriteLine("Se ha establecido correctamente la conexion a la BD");
            }
            catch { Console.WriteLine("Ha ocurrido un error al intentar establecer conexion con la BD"); }
            
        }

        public void close ()
        {
            this.coneccion.Close();
        }

        public DataTable Consultar_Datos(string sql)
        {
            this.open();

            DataTable Datos_Obtenidos = new DataTable();
            comando = coneccion.CreateCommand();
            comando.Connection = coneccion;
            comando.CommandText = sql;
            read = comando.ExecuteReader();
            Datos_Obtenidos.Load(read);
            read.Close();
            this.close();
            return Datos_Obtenidos;
        }
    }
}
