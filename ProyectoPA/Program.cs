﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Npgsql;
using NpgsqlTypes;


namespace ProyectoPA
{
    class Program
    {
        static void Main(string[] args)
        {
            NpgsqlConnection ConexionPG = new NpgsqlConnection("Server = localhost; Port = 5432; Database = postgres; User ID = postgres; Password = 1234");

            ConexionGenerica portconnection = new ConexionGenerica(ConexionPG);

            DataTable mytable = portconnection.Consultar_Datos("select * from proyecto");

            foreach(DataRow fila in mytable.Rows)
            {

                Console.WriteLine(" ID : " + fila[0] + " Nombre: " + fila[1]);

            }

            Console.ReadKey();

        }
    }
}
